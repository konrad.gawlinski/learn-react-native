import React, {Component} from 'react';
import { StyleSheet, Modal, View, ScrollView, FlatList, Text, TextInput, Button, TouchableOpacity } from 'react-native';

class App extends Component {
  constructor (props) {
    super(props);

    this.state = {
      providedGoal: '',
      storedGoals: [],
      isNewGoalModalVisible: false
    };

    this.onProvidedGoalChange = this.onProvidedGoalChange.bind(this);
    this.openNewGoalModal = this.openNewGoalModal.bind(this);
    this.closeNewGoalModal = this.closeNewGoalModal.bind(this);
    this.addGoal = this.addGoal.bind(this);
    this.renderGoalItem = this.renderGoalItem.bind(this);
    this.removeGoal = this.removeGoal.bind(this);
  }

  onProvidedGoalChange (providedText) {
    this.setState({
      providedGoal: providedText
    });
  }

  openNewGoalModal () {
    this.setState({
      isNewGoalModalVisible: true
    });
  }

  closeNewGoalModal () {
    this.setState({
      isNewGoalModalVisible: false
    });
  }

  addGoal () {
    this.setState(state => ({
      storedGoals: [
        ...state.storedGoals,
        { key: Math.random().toString(), title: state.providedGoal }
      ]
    }));
    this.closeNewGoalModal();
  }

  removeGoal (goalKey) {
    const filteredGoals = this.state.storedGoals.filter((goal) => {
      return goal.key !== goalKey;
    });

    this.setState({
      storedGoals: filteredGoals
    });
  }

  render () {
    return (
      <View>
        <Button title="Create New Goal" onPress={this.openNewGoalModal}/>
        <Modal visible={this.state.isNewGoalModalVisible}>
          <View style={{flexDirection: 'row', alignItems:'stretch', flex:1}}>
          <View style={style.inputModal}>
            <TextInput onChangeText={this.onProvidedGoalChange} style={style.goalInput}/>
            <Button title="Add" onPress={this.addGoal}/>
            <TouchableOpacity style={{alignItems: 'center', backgroundColor: 'red', padding: 10}} onPress={this.closeNewGoalModal}>
              <Text style={{color: 'white'}}>Cancel</Text>
            </TouchableOpacity>
          </View>
          </View>
        </Modal>

        <FlatList
          style={style.goalsList}
          data={this.state.storedGoals}
          renderItem={this.renderGoalItem}
        />
      </View>
    );
  }

  renderGoalItem ({item}) {
    return (
      <TouchableOpacity activeOpacity={0.8} onPress={() => this.removeGoal(item.key)}>
        <Text style={style.goalItem}>{item.title}</Text>
      </TouchableOpacity>
    );
  }
}

const style = StyleSheet.create({
  inputModal: {
    padding: 10,
    justifyContent: 'center',
    flex: 1
  },

  goalInput: {
    padding: 5,
    borderWidth: 1
  },

  goalsList: {
    padding: 5
  },

  goalItem: {
    margin: 5,
    padding: 10,
    backgroundColor: 'lightgray',
    borderColor: 'gray'
  }
});

export default App;
